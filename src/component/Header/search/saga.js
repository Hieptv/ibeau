import { takeLatest, put } from "redux-saga/effects";
import { ActionSearch, searchSuccess, searchError } from "./actions";

function* SagagetSearch(search) {
    const token = localStorage.getItem('isLogin');
    if (search)
        try {
            const reqSearch  = yield fetch(`https://192.168.0.144:3002/api/v1/user/search?q=${search.payload}`,{
                method: "GET",
                headers: new Headers({
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'Authorization': `Bearer ${token}`
                })

            })
            const resSearch = yield reqSearch.json();
            
            yield put(searchSuccess(resSearch.data));
        }

        catch (error) {
            yield put(searchError(error));
        }
}
export default function* watchSagasearch() {
    yield takeLatest(ActionSearch.SEARCH, SagagetSearch)
}