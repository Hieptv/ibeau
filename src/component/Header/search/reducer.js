import {ActionSearch} from './actions';

const search = {
    q : ''
}

const getSearch = (state = search, action) => {
    switch(action.type) {
        case ActionSearch.SEARCH: {
            return{
                ...state
            }
        }
        case ActionSearch.SEARCH_SUCCESS: {
            return {
                ...state,
                q: action.payload
            }
        }
        case ActionSearch.SEARCH_ERROR: {
            return {
                ...state
            }
        }
        default: return {
            ...state
        };
    }
};

export default getSearch;