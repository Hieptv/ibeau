export const ActionSearch = {
    SEARCH: 'SEARCH',
    SEARCH_SUCCESS: 'SEARCH_SUCCESS',
    SEARCH_ERROR: "SEARCH_ERROR"
}

export const search = (list) => {
    return {
        type: ActionSearch.SEARCH,
        payload: list
    }
}

export const searchSuccess = (list) => {
    return {
        type: ActionSearch.SEARCH_SUCCESS,
        payload: list
    }
}

export const searchError = (list) => {
    return {
        type: ActionSearch.SEARCH_ERROR,
        payload: list
    }
}