import React, { useEffect, useState } from 'react';
import './style.scss'
import { Navbar, NavDropdown, Nav, Form, Button, FormControl } from 'react-bootstrap';
import { Link, Route, useHistory } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import PrivateRoute from '../Home/PrivateRoute';
import Login from '../Login/Login';
import Home from '../Home';
import '../Header/style.scss';
import { useSelector, useDispatch } from 'react-redux';
import io from 'socket.io-client';
import listUser from '../Home/reducer';

const Header = () => {
    const history = useHistory();
const signout = (e) =>{
   if(window.confirm("Bạn có chắc muốn đăng xuất?")) {
       localStorage.removeItem("isLogin");
       history.push("/login")
   }
    
}


const home = (e) => {
    history.push("/")
}

const dispatch = useDispatch();
const user_id = localStorage.getItem('id');
const [datas, setDatas] = useState([]);
useEffect(()=>{ 
    const socket = io(`http://e25bfec37055.ngrok.io/`)
    socket.emit('logout', {user_id: user_id})
    socket.on('list-user', (datass)=>{
       setDatas(datass)
    })
},[])


useEffect(()=>{
    if(datas.length) return dispatch(listUser(datas))
},[])

    
    return (
        
        <Navbar expand="lg" className="header">
            <Navbar className="container">
                <Navbar.Brand href="#home"><img src="/images/logo.png" /></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto menu">
                        <Nav.Link to="/" className="menu-link" onClick= {home}>Home</Nav.Link>
                        <Nav.Link to="/service/dom" className="menu-link">Link</Nav.Link>
                        <NavDropdown title="Option" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.3"><Link to="/setting">Setting</Link></NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.4"><Button variant="link p-0 logout" onClick={signout}>Log out</Button></NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className=" mr-sm-2" />
                        <Button type="submit">Submit</Button>
                    </Form>
                    <Switch>
                        <Route exact path="/login">
                            <Login />
                        </Route>
                        <PrivateRoute path="/home" component={Home} />
                    </Switch>
                </Navbar.Collapse>
            </Navbar>
        </Navbar >
       
    )
};
export default Header;