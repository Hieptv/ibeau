import {ActionGetuserid, getuseriderror, getuseridsuccess} from './actions';
import { put, takeLatest } from 'redux-saga/effects';

function* Sagagetuserid(userid) {
    const token = localStorage.getItem('isLogin');
    if(userid){
        try{
            const requestuserid = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/get-user?user_id=${userid.payload}`,{
                method: "GET",
                headers: new Headers({
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'Authorization' : `Bearer ${token}`
                })
            })
            const responseUserid = yield requestuserid.json();
            if(responseUserid.status === 200){
                localStorage.setItem('id', responseUserid.data?._id);
            }
            yield put(getuseridsuccess(responseUserid.data));
        }
        catch(error){
            yield put(getuseriderror)
        }
    }
}
export default function* watchSagauserid() {
    yield takeLatest(ActionGetuserid.GET_USERID, Sagagetuserid)
}
