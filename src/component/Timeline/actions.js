export const ActionGetuserid = {
    GET_USERID : 'GET_USERID',
    GET_USERID_SUCCESS : 'GET_USERID_SUCCESS',
    GET_USERID_ERROR : 'GET_USERID_ERROR',
}

export const getuserid = (list) =>{
    return {
        type: ActionGetuserid.GET_USERID,
        payload: list
    }
}
export const getuseridsuccess = (list) =>{
    return {
        type: ActionGetuserid.GET_USERID_SUCCESS,
        payload: list
    }
}
export const getuseriderror = (list) => {
    return {
        type: ActionGetuserid.GET_USERID_ERROR,
        payload: list
    }
}