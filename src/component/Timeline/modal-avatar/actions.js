export const ActionImage = {
    AVATAR : 'AVATAR',
    AVATAR_SUCCESS : 'AVATAR_SUCCESS',
    AVATAR_ERROR : 'AVATAR_ERROR',

    UPDATE_AVATAR: 'UPDATE_AVATAR',
    UPDATE_AVATAR_SUCCESS: 'UPDATE_AVATAR_SUCCESS',
    UPDATE_AVATAR_ERROR: 'UPDATE_AVATAR_ERROR'
}

export const avatar = (list) => {
    return {
        type: ActionImage.AVATAR,
        payload: list
    }
}
export const avatarSuccess = (list) => {
    return {
        type: ActionImage.AVATAR_SUCCESS,
        payload: list
    }
}

export const avatarError = (list) => {
    return {
        type: ActionImage.AVATAR_ERROR,
        payload: list
    }
}


export const updateAvatar = (list) => {
    return {
        type: ActionImage.UPDATE_AVATAR,
        payload: list
    }
}

export const updateAvatarSuccess = (list) => {
    return {
        type: ActionImage.UPDATE_AVATAR_SUCCESS,
        payload: list
    }
}

export const updateAvatarError = (list) => {
    return {
        type: ActionImage.UPDATE_AVATAR_ERROR,
        payload: list
    }
}
