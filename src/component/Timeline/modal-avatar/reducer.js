import { ActionImage} from './actions';

const upAvatar = {
    avatar : ''
}

const loadAvatar = (state = upAvatar , action ) => {
    switch(action.type) {
        case ActionImage.AVATAR: {
            return {
                ...state
            }
        }
        case ActionImage.AVATAR_SUCCESS: {
            return {
                ...state
            }
        }
        case ActionImage.AVATAR_ERROR: {
            return {
                ...state
            }
        }
        default : return {...state}
    }
}

export default loadAvatar;