import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Modal, Button, FormControl, Spinner } from 'react-bootstrap';
import {avatar} from './actions'
import '../../Timeline/style.scss'

function Avatar (props) {
    const [file , setfile] = useState('');
    const dispatch = useDispatch();
    const loadPage = useSelector(state => state.getuserID);
    const upLoad = () => {
        let formData = new FormData();
        formData.append('single_image_training',file)
        if(file){
            dispatch(avatar(formData));
            props.onHide()
            
        }
    }
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                Upload Avatar
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <FormControl
                type="file"
                onChange={(e) => setfile(e.target.files[0])}
                />
            </Modal.Body>
            {loadPage.loadpage === true?  <Spinner animation="border" variant="secondary" />:""}
            <Modal.Footer>
                <Button onClick={upLoad}>Upload</Button>
                <Button onClick={props.onHide}>close</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default Avatar
