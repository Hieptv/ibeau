import { ActionImage } from "./actions";
import { takeLatest, call, put } from "redux-saga/effects";
import { getuserid } from "../actions";
import axios from "axios";
import { updateAvatar } from "./actions";
const qs = require('querystring');


const { default: loadAvatar } = require("./reducer")



const token = localStorage.getItem('isLogin');

function* SagaLoadAvatar(user) {
    console.log(user.payload);
    try {
        const res = yield axios.post(`https://brand.spify.io/r3st/api/v1/upload-single-image`, user.payload, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (res.data.status === true) {
            yield put(updateAvatar(res.data.location))
        }
    }
    catch (error) {

    }
}

function* SagaUpdateAvatar(location) {
    console.log(location);
    try {
        const token = localStorage.getItem('isLogin');
        const id = localStorage.getItem('id');
        const res = yield axios.put(`https://social-aht.herokuapp.com/api/v1/user/update-avatar/${id}`, qs.stringify({ location: location.payload }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': '*/*',
                'Authorization': `Bearer ${token}`
            }
        })
        console.log(res)
        if (res.status === 200) {
            yield put(getuserid(id))
        }
    }
    catch (error) {

    }
}

export default function* watchSagaLoadAvatar() {
    yield takeLatest(ActionImage.AVATAR, SagaLoadAvatar)
    yield takeLatest(ActionImage.UPDATE_AVATAR, SagaUpdateAvatar)

}