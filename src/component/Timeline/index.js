import React, { useEffect, useState } from 'react';
import { useLocation, Link, Route } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Header from '../Header';
import '../Timeline/style.scss'
import { getuserid } from './actions';
import {follow} from './follow/actions';
import About from './About/About';
import PostDetail from '../Home/PostDetail';
import { Button, Spinner, Card } from 'react-bootstrap';
import Avatar from './modal-avatar';
import ImageCover from './modal-imageCover';
import Information from './information';
import Story from './story';
import jwt_decode from 'jwt-decode';
import AllUser from '../Home/getAllUser/index';

export default function Timeline() {
    const [modalShow, setModalShow] = React.useState(false);
    const [showfollowings, setshowfollowings] = React.useState(false);
    const [isFollow, setIsFllow] = useState('')
    const [showcoverImg, setshowcoverImg] = React.useState(false);
    const [showInformation, setshowInformation] = React.useState(false);
    const [showStory, setshowStory] = React.useState(false);
    const data = useSelector(state => state.getuserID.listuserid);
    const loadPage = useSelector(state => state.getuserID);
    const upLoadImageCover = useSelector(state => state.getuserID);
    var datarender = "";
    var urlImagecover  = "";
    if (data)
        datarender = (
            <>

                <a href="#">
                    <img src={data.avatar !== "" ? data.avatar : data.avatar === "" && data.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="img-responsive profile-photo" />{loadPage.loadpage === true ? <Spinner animation="border" variant="primary" /> : ""}
                </a>
                <h3 className="timeline-fullname">{data.firstName} {data.lastName}</h3>
                <p className="text-muted">Creative Director</p>
            </>
        )
    else datarender = <></>
    
    if(data)
        urlImagecover  = (
            <>
                <a href="#">
                    <img src={data.coverImage !== "" ? data.coverImage : "/images/covers/1.jpg"} />{upLoadImageCover.loadpage === true?  <Spinner animation="border" variant="secondary" />:""}
                </a>
            </>
        ) 
    else  urlImagecover = <></>

    var relationship = "";
    if(data.relationship === 0){
        relationship = (
            <>
                <p>relationship: Alone</p>
            </>
        )
    }
    else if(data.relationship === 1){
        relationship = (
            <>
                <p>relationship: Married</p>
            </>
        )
    }
    else if(data.relationship === 2){
        relationship = (
            <>
                <p>relationship: Unknown</p>
            </>
        )
    }
    
    var gender = "";
    if(data.gender === 0){
        gender =(
            <>
                <p>gender: FeMale</p>
            </>
        )
    }
    else if(data.gender === 1){
        gender =(
            <>
                <p>gender: Male</p>
            </>
        )
    }

    let query = new URLSearchParams(useLocation().search);
    const userid = query.get("userid")
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getuserid(userid))
    }, [userid])


    const token = localStorage.getItem('isLogin');
    var decoded = jwt_decode(token);
    localStorage.setItem('id',decoded.userId);
    const userID = decoded.userId

    const hanldchangeFollow = () => {
        if(userid == ''){
            return alert('userid không có')
        }
        else{
            dispatch(follow(userid))
        }
    }
    var Follows = "";
    var story = "";
    var information ="";
    var upAvatar ="";
    var upImageCover =""
    if(userID != userid){
        Follows = (
            <li ><Button variant="light" onClick={hanldchangeFollow}>follow</Button> </li>
        )
        story = <></>
        information = <></>
        upAvatar =<></>
        upImageCover = <></>
    }else {
        Follows = <></>
        story = (
            <Card.Link href="#" style={{color: 'blue'}} onClick={() => setshowStory(true) }>Thêm tiểu sử</Card.Link>
        )
        information = (
            <Button variant="primary" size="lg" block onClick={() => setshowInformation(true)}>
            Chỉnh sửa chi tiết
            </Button>
        )
        upAvatar = (
            <Button className="btn-upload" type="button" variant="link" 
            onClick={() => setModalShow(true)} data-toggle="tooltip" data-placement="bottom" title="Upload Avatar">
            <i className="fa fa-camera" aria-hidden="true"></i>
            </Button>
        )
        upImageCover =(
            <Button className="upload_cover" variant="link"
            data-toggle="tooltip" data-placement="bottom" onClick={() => setshowcoverImg(true)}
            title="Upload Image Cover">
            <i className="fa fa-camera" aria-hidden="true"></i>
            </Button>
        )
    }


    return (
        <div className="timline">
            <Header />
            <div className="container">
                <div className="timeline">
                    <div className="timeline-cover">

                        <div className="timeline-nav-bar">
                            <div className="row">
                                <div className="col-md-3">
 
                                    <div className="profile-info">
                                        {datarender}
                                        {upAvatar}
                                        <Avatar show={modalShow} userid={userid} onHide={() => setModalShow(false)} />
                                    </div>
                                </div>
                                <div className="col-md-9">
                                    <ul className="list-inline profile-menu">
                                        <li><Link to={`/timeline/?userid=${data?._id}`}>Timeline</Link></li>
                                        <li><Link to={`/timeline/about?userid=${data?._id}`}>About</Link></li>
                                        <li><a href="timeline-album.html">Album</a></li>
                                        <li><a href="timeline-friends.html">Friends</a></li>
                                        <li >{Follows}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="Image-cover">
                        {urlImagecover}
                    </div>
                    <div className="btn-uploadCoverImage">
                        {upImageCover}
                        <ImageCover show={showcoverImg} userid={userid} onHide={() => setshowcoverImg(false)} />
                    </div>
                    <div>

                    </div>
                    <div id="page-contents">
                        <div className="row">
                            <div className="col-md-3" >
                            
                                <Card style = {{width: '16rem'}}>
                                    <Card.Body >
                                        <Card.Title>Information</Card.Title>
                                            <Card.Body>
                                                <p>{data.story}</p>
                                            </Card.Body>
                                           {story}
                                            <Story show={showStory} userid={userid} onHide={() => setshowStory(false)} />
                                        <Card.Text>
                                            <p>email: {data.email} </p>
                                            <p>phone: {data.phone} </p>
                                            <p>birthday: {data.birthday} </p>
                                            <p>address: {data.address} </p>
                                            {relationship}
                                            {gender}

                                        </Card.Text>
                                        {information}
                                        <Information show={showInformation} userid={userid} onHide={() => setshowInformation(false)} />
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="col-md-7">
                                <Route exact path="/timeline/" component={PostDetail} />
                                <Route exact path="/timeline/about" component={About} />
                            </div>
                            <div className="col-md-3 static">
                                <div id="sticky-sidebar">
                                    <h4 className="grey">User follow</h4>
                                    <AllUser></AllUser>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    )
}