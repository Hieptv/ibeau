export const ActionInformation = {
    INFORMATION: 'INFORMATION',
    INFORMATION_SUCCESS: 'INFORMATION_SUCCESS',
    INFORMATION_ERROR: 'INFORMATION_ERROR',
}

export const information = (list) => {
    return {
        type: ActionInformation.INFORMATION,
        payload: list
    }
}
export const informationSuccess = (list) => {
    return {
        type: ActionInformation.INFORMATION_SUCCESS,
        payload: list
    }
}

export const informationError = (list) => {
    return {
        type: ActionInformation.INFORMATION_ERROR,
        payload: list
    }
}


