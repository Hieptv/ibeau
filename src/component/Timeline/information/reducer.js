import { ActionInformation} from './actions';

const upInformation = {
    list : [],

}

const loadInformation = (state = upInformation , action ) => {
    switch(action.type) {
        case ActionInformation.INFORMATION: {
            return {
                ...state
            }
        }
        case ActionInformation.INFORMATION_SUCCESS: {
            return {
                ...state
            }
        }
        case ActionInformation.INFORMATION_ERROR: {
            return {
                ...state
            }
        }
        default : return {...state}
    }
}

export default loadInformation;