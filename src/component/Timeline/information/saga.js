import { ActionInformation } from "./actions";
import { takeLatest, put } from "redux-saga/effects";

import loadInformation from "./reducer";

const user_id = localStorage.id;
const token = localStorage.isLogin;


function* SagaInformation(user) {
        console.log(user)
        try{
            const req = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/update/${user_id}`,{
                method: 'PUT',
                headers:{
                    'Authorization' : `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "email": user.payload.email, 
                    "phone" : user.payload.phone,
                    "birthday" : user.payload.birthday,
                    "address": user.payload.address,
                    "relationship": user.payload.relationship,
                    "gender" : user.payload.gender,
                })
            })
            const res = yield req.json();
            console.log(res)
            if (res.status === 200) {
                yield put(loadInformation(user))
            }
        }
        catch(error){
            // yield put(informationError)
        }
}
export default function* watchSagaInformation() {
    yield takeLatest(ActionInformation.INFORMATION, SagaInformation)
}
