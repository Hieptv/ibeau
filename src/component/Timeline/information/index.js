import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { Modal, Button,  Form, Row, Col } from 'react-bootstrap';
import {information} from './actions';
import '../../Timeline/style.scss'

function Information(props){
    const [email, setemail] = useState('');
    const [phone, setphone] = useState('');
    const [birthday, setbirthday] = useState('');
    const [address, setaddress] = useState('');
    const [relationship, setrelationship] = useState('');
    const [gender, setgender] = useState('');

    const dispatch = useDispatch()

    
    const handleUpdate = () => {
        if(email === '', phone === '', birthday === '', address === '', relationship === '', gender ===''){
            alert('Bạn chưa điền vào ô trống')
        }
        else{
            dispatch(information({email: email, phone: phone, birthday: birthday, address: address, relationship: relationship, gender: gender}));
            props.onHide()
        }

    }

    return(
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                Update Information
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                    email
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="email" onChange={e => setemail(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                    phone
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setphone(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                    birthday
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="date" onChange={e => setbirthday(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                    address
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setaddress(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group className="d-flex"> relationship : 
                    <Form.Check className="mr-3" type="checkbox" label="Unknown" onChange={(e) => setrelationship('2')} />
                    <Form.Check className="mr-3" type="checkbox" label="Married" onChange={(e) => setrelationship('1')} />
                    <Form.Check type="checkbox" label="Alone" onChange={(e) => setrelationship('0')} />
                </Form.Group>
                <Form.Group className="d-flex"> gender : 
                    <Form.Check className="mr-3" type="checkbox" label="Male" onChange={(e) => setgender('1')} />
                    <Form.Check type="checkbox" label="FeMale" onChange={(e) => setgender('0')} />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleUpdate}>Update</Button>
                <Button onClick={props.onHide}>close</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default Information