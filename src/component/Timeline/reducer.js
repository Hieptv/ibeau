import {ActionGetuserid} from './actions';

const userid = {
    listuserid: [],
    loadpage: false,
    location: ''
}

const getuserID = (state = userid, action) => {
    switch(action.type){
        case ActionGetuserid.GET_USERID: {
            return {
                ...state,
                loadpage: true
            }
        }
        case ActionGetuserid.GET_USERID_SUCCESS: {
            return {
                ...state,
                listuserid: action.payload,
                loadpage: false,
                location: action.payload
            }
        }
        case ActionGetuserid.GET_USERID_ERROR: {
            return {
                ...state
            }
        }
        default: return{...state}
    }
}

export default getuserID

