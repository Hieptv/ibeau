export const ActionFollow = {
    FOLLOW: 'FOLLOW',
    FOLLOW_SUCCESS: 'FOLLOW_SUCCESS',
    FOLLOW_ERROR: 'FOLLOW_ERROR',
}

export const follow = (list) => {
    return {
        type: ActionFollow.FOLLOW,
        payload: list
    }
}
export const followSuccess = (list) => {
    return {
        type: ActionFollow.FOLLOW_SUCCESS,
        payload: list
    }
}

export const followError = (list) => {
    return {
        type: ActionFollow.FOLLOW_ERROR,
        payload: list
    }
}


