import { ActionFollow} from './actions';

const followfriend = {
    userid : ''
}

const followFriend = (state = followfriend , action ) => {
    switch(action.type) {
        case ActionFollow.FOLLOW: {
            return {
                ...state
            }
        }
        case ActionFollow.FOLLOW_SUCCESS: {
            return {
                ...state
            }
        }
        case ActionFollow.FOLLOW_ERROR: {
            return {
                ...state
            }
        }
        default : return {...state}
    }
}

export default followFriend;