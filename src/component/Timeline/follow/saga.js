import { ActionFollow, followSuccess } from "./actions";
import { takeLatest, put } from "redux-saga/effects";
import followFriend from "./reducer";

const token = localStorage.isLogin;


function* SagaFollow(user) {
    console.log(user)
        try{
            const req = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/follow-user/${user.payload}`,{
                method: 'PUT',
                headers:{
                    'Authorization' : `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            })
            const res = yield req.json();
            console.log(res)
            if (res.state === true) {
                yield put(followFriend(user.payload.userid))
            }
        }
        catch(error){
            // yield put(informationError)
        }
}


export default function* watchSagaFollow() {
    yield takeLatest(ActionFollow.FOLLOW, SagaFollow)
}
