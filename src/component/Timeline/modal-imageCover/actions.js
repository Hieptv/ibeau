export const ActionImageCover = {
    IMAGE_COVER : 'IMAGE_COVER',
    IMAGE_COVER_SUCCESS : 'IMAGE_COVER_SUCCESS',
    IMAGE_COVER_ERROR : 'IMAGE_COVER_ERROR',

    UPDATE_IMAGE_COVER: 'UPDATE_IMAGE_COVER',
    UPDATE_IMAGE_COVER_SUCCESS: 'UPDATE_IMAGE_COVER_SUCCESS',
    UPDATE_IMAGE_COVER_ERROR: 'UPDATE_IMAGE_COVER_ERROR'
}

export const imageCover = (list) => {
    return {
        type: ActionImageCover.IMAGE_COVER,
        payload: list
    }
}

export const imageCoverSuccess = (list) => {
    return {
        type: ActionImageCover.IMAGE_COVER_SUCCESS,
        payload: list
    }
}

export const imageCoverError = (list) => {
    return {
        type: ActionImageCover.IMAGE_COVER_ERROR,
        payload: list
    }
}

export const updateImageCover = (list) => {
    return {
        type: ActionImageCover.UPDATE_IMAGE_COVER,
        payload: list
    }
}

export const updateImageCoverSuccess = (list) => {
    return {
        type: ActionImageCover.UPDATE_IMAGE_COVER_SUCCESS,
        payload: list
    }
}
export const updateImageCoverError = (list) => {
    return {
        type: ActionImageCover.UPDATE_IMAGE_COVER_ERROR,
        payload: list
    }
}