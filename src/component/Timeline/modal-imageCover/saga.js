import { ActionImageCover, updateImageCover } from "./actions";
import { takeLatest, call, put } from "redux-saga/effects";
import { getuserid } from "../actions";
import axios from "axios";
import qs from 'qs';



const token = localStorage.getItem('isLogin');


function* SagaLoadImageCover(user){
    console.log(user.payload)
    try {
        const requestImageCover = yield axios.post(`https://brand.spify.io/r3st/api/v1/upload-single-image`, user.payload, {
            headers : {
                'Content-Type': 'multipart/form-data',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if(requestImageCover.data.status === true){
            yield put(updateImageCover(requestImageCover.data.location))
        }
    }
    catch(error){
        
    }
}

function* SagaUpdateImageCover(location) {
    console.log(location);
    try {
        const token = localStorage.getItem('isLogin');
        const id = localStorage.getItem('id');
        const res = yield axios.put(`https://social-aht.herokuapp.com/api/v1/user/update-cover-image/${id}`, qs.stringify({ location: location.payload }), {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': '*/*',
                'Authorization': `Bearer ${token}`
            }
        })
        console.log(res)
        if (res.status === 200) {
            yield put(getuserid(id))
        }
    }
    catch (error) {

    }
}

export default function* watchSagaLoadImageCover() {
    yield takeLatest(ActionImageCover.IMAGE_COVER, SagaLoadImageCover)
    yield takeLatest(ActionImageCover.UPDATE_IMAGE_COVER, SagaUpdateImageCover)
}