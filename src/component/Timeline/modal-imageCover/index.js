import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { imageCover } from '../modal-imageCover/actions';
import { Modal, FormControl, Button, Spinner } from 'react-bootstrap';

function ImageCover(props) {
    const [file , setfile] = useState('');
    const dispatch = useDispatch();
    const upLoadImageCover = useSelector(state => state.getuserID);
    let formImageCover = new FormData();
    formImageCover.append('single_image_training', file);
    const LoadImageCover = () => {
        if(file){
            dispatch(imageCover(formImageCover));
            props.onHide();
        }
    }
    return (
        <Modal
          {...props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Upload Image Cover
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormControl
              type="file"
              onChange={(e) => setfile(e.target.files[0])}
            />
          </Modal.Body>
          {upLoadImageCover.loadpage === true?  <Spinner animation="border" variant="secondary" />:""}
          <Modal.Footer>
            <Button onClick={LoadImageCover}>Upload</Button>
            <Button onClick={props.onHide}>close</Button>
          </Modal.Footer>
        </Modal>
    )
}

export default ImageCover;
