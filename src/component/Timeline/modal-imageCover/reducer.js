import { ActionImageCover} from './actions';

const upImageCover = {
    coverImage : []
}

const loadImageCover = (state = upImageCover , action ) => {
    switch(action.type) {
        case ActionImageCover.IMAGE_COVER: {
            return {
                ...state
            }
        }
        case ActionImageCover.IMAGE_COVER_SUCCESS: {
            return {
                ...state
            }
        }
        case ActionImageCover.IMAGE_COVER_ERROR: {
            return {
                ...state
            }
        }
        default : return {...state}
    }
}

export default loadImageCover;