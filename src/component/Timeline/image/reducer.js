import {ActionUpImage} from './actions';

const upImage = {
    location: '',
}

const putImage = (state = upImage, action) => {
    switch(action.type){
        case ActionUpImage.IMAGE: {
            return {
                ...state,
                
            }
        }
        case ActionUpImage.IMAGE_SUCCESS: {
            return {
                ...state,
                location: action.payload,
                
            }
        }
        case ActionUpImage.IMAGE_ERROR: {
            return {
                ...state
            }
        }
        default: return{...state}
    }
}

export default putImage

