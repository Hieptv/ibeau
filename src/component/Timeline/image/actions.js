export const ActionUpImage = {
    IMAGE : 'IMAGE',
    IMAGE_SUCCESS : 'IMAGE_SUCCESS',
    IMAGE_ERROR : 'IMAGE_ERROR',

    
}

export const image = (list) => {
    return {
        type: ActionUpImage.IMAGE,
        payload: {
            list
        }
    }
}
export const imageSuccess = (list) => {
    return {
        type: ActionUpImage.IMAGE_SUCCESS,
        payload: list
    }
}

export const imageError = (list) => {
    return {
        type: ActionUpImage.IMAGE_ERROR,
        payload: list
    }
}

