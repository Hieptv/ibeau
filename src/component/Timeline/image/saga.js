import {ActionUpImage, imageSuccess, imageError} from './actions';
import { put, takeLatest } from 'redux-saga/effects';

function* Sagaputimage(upImage) {
   
    if(upImage){
        try{
            const requestImage = yield fetch(`https://brand.spify.io/r3st/api/v1/upload-single-image`,{
                method: "GET",
                headers: new Headers({
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                   
                })
            })
            const responseImage = yield requestImage.json();
            if(responseImage.status === true){
                localStorage.setItem('loaction', responseImage.data.location);
            }
            yield put(imageSuccess(responseImage.data));
        }
        catch(error){
            yield put(imageError)
        }
    }
}
export default function* watchSagaupimage() {
    yield takeLatest(ActionUpImage.IMAGE, Sagaputimage)
}
