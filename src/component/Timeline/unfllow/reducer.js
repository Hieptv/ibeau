import { ActionUnFollow} from './actions'

const unfollowfriend = {
    userid : ''
}

const unfollowFriend = (state = unfollowfriend , action ) => {
    switch(action.type) {
        case ActionUnFollow.UNFLLOW: {
            return {
                ...state
            }
        }
        case ActionUnFollow.UNFOLLOW_SUCCESS: {
            return {
                ...state
            }
        }
        case ActionUnFollow.UNFOLLOW_ERROR: {
            return {
                ...state
            }
        }
        default : return {...state}
    }
}

export default unfollowFriend;