import { ActionUnFollow, } from "./actions";
import { takeLatest, put } from "redux-saga/effects";
import unfollowFriend from "./reducer";

const token = localStorage.isLogin;


function* SagaUnFollow(user) {
    console.log(user)
        try{
            const req = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/unfollow-user/${user.payload}`,{
                method: 'PUT',
                headers:{
                    'Authorization' : `Bearer ${token}`,
                    'Content-Type': 'application/json',
                }
            })
            const res = yield req.json();
            console.log(res)
            if (res.state === true) {
                yield put(unfollowFriend(user.payload.userid))
            }
        }
        catch(error){
            // yield put(informationError)
        }
}


export default function* watchSagaUnFollow() {
    yield takeLatest(ActionUnFollow.UNFOLLOW, SagaUnFollow)
}
