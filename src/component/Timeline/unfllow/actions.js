export const ActionUnFollow = {
    UNFLLOW: 'UNFLLOW',
    UNFLLOW_SUCCESS: 'UNFLLOW_SUCCESS',
    UNFLLOW_ERROR: 'UNFLLOW_ERROR'
}

export const unfllow = (list) => {
    return {
        type: ActionUnFollow.UNFLLOW,
        payload: list
    }
}

export const unfllowSuccess = (list) => {
    return {
        type: ActionUnFollow.UNFLLOW_SUCCESS,
        payload: list
    }
}

export const unfllowError = (list) => {
    return {
        type: ActionUnFollow.UNFLLOW_ERROR,
        payload: list
    }
}