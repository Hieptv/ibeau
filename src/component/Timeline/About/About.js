import React from 'react';
import { useLocation } from 'react-router';
import { useSelector } from 'react-redux';
import { Table } from 'react-bootstrap';
import './style.scss';

function About() {
    let query = new URLSearchParams(useLocation().search);
    const userid = query.get("userid");
    const lists = useSelector(state => state.getuserID.listuserid);
    var infordetail = '';
    if(lists){
        infordetail = (
            <div> 
                <Table className="mt-3 table-infor" size="md">

                    <tbody>
                        <tr>
                            <td><i class="fas fa-birthday-cake"></i> Birthday:</td>
                            <td>{lists.birthday}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-map-marker-alt"></i> Address:</td>
                            <td>{lists.address}</td>

                        </tr>
                        <tr>
                            <td><i class="fas fa-genderless"></i> Gender:</td>
                            <td >{lists.gender === 1 ? "Male" : "Female"}</td>
                        </tr>
                        <tr>
                            <td><i class="far fa-envelope"></i> Email:</td>
                            <td >{lists.email}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-phone"></i> Phone:</td>
                            <td >{lists.phone}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-phone"></i> Relationship:</td>
                            <td >{lists.relationship}</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        )
    }
    else infordetail = <></>
    return (
        <div className="pt-3">
            <h3>Information Details</h3>
            {infordetail}
        </div>
    )
}

export default About
