import { ActionStory} from './actions';

const upStory = {
    story : '',
}

const loadStory = (state = upStory , action ) => {
    switch(action.type) {
        case ActionStory.STORY: {
            return {
                ...state
            }
        }
        case ActionStory.STORY_SUCCESS: {
            return {
                ...state,
                story: action.payload
            }
        }
        case ActionStory.STORY_ERROR: {
            return {
                ...state
            }
        }
        default : return {...state}
    }
}

export default loadStory;