import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { Modal, Button,  Form, Row, Col } from 'react-bootstrap';
import {upStory} from './actions';
import '../../Timeline/style.scss'

function Story(props){
    const [story, setstory] = useState('');
    
    const dispatch = useDispatch()

    
    const handleUpdate = () => {
        if(story === ''){
            alert('bạn chưa điền tiểu sử cần thay đổi')
        }
        else{
            dispatch(upStory({story: story}));
            props.onHide()
        }
    }
    return(
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                Update Story
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                    story
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setstory(e.target.value)} />
                    </Col>
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleUpdate}>Update</Button>
                <Button onClick={props.onHide}>close</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default Story