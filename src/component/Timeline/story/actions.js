export const ActionStory = {
    STORY: 'STORY',
    STORY_SUCCESS: 'STORY_SUCCESS',
    STORY_ERROR: 'STORY_ERROR',
}

export const upStory = (list) => {
    return {
        type: ActionStory.STORY,
        payload: list
    }
}
export const upStorySuccess = (list) => {
    return {
        type: ActionStory.STORY_SUCCESS,
        payload: list
    }
}

export const upStoryError = (list) => {
    return {
        type: ActionStory.STORY_ERROR,
        payload: list
    }
}


