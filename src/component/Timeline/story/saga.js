import { ActionStory} from "./actions";
import { takeLatest,  put } from "redux-saga/effects";
import loadStory from "./reducer";

const user_id = localStorage.id;
const token = localStorage.isLogin;


function* SagaStory(user) {
    console.log(user)
        try{
            const req = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/update-story/${user_id}`,{
                method: 'PUT',
                headers:{
                    'Authorization' : `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    story: user.payload.story,
                })
            })
            const res = yield req.json();
            if (res.status === 200) {
                yield put(loadStory(user.payload.story))
            }
        }
        catch(error){
        }
}
export default function* watchSagaStory() {
    yield takeLatest(ActionStory.STORY, SagaStory)
}
