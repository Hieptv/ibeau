import React from 'react';
import '../Message/style.scss';
// import ListUser from './listUser/index';
// import Room from './chat/index';

export default function Message() {
    return (
        <div className="create-post">
            <div className="row">
                <div className="col-md-7 col-sm-7">
                    <div className="form-group">
                        {/* <img src="images/users/user-1.jpg" alt className="profile-photo-md" /> */}
                        <textarea name="texts" id="exampleTextarea" cols={30} rows={1} className="form-control" placeholder="Write what you wish" defaultValue={""} />
                    </div>
                </div>
                <div className="col-md-5 col-sm-5">
                    <div className="tools">
                        <ul className="publishing-tools list-inline">
                            <li><a href="#"><i className="ion-compose" /></a></li>
                            <li><a href="#"><i className="ion-images" /></a></li>
                            <li><a href="#"><i className="ion-ios-videocam" /></a></li>
                            <li><a href="#"><i className="ion-map" /></a></li>
                        </ul>
                        <button className="btn btn-primary pull-right">Publish</button>
                    </div>
                </div>
            </div>
            <div className="chat-room">
                <div className="row">
                    {/* <ListUser /> */}
                    <div className="col-md-7">
                        {/*Chat Messages in Right*/}
                        {/* <Room /> */}
                        <div className="tab-content scrollbar-wrapper wrapper scrollbar-outer">
                <div className="tab-pane active" id="contact-1">
                    <div className="chat-body">
                        <ul className="chat-message">
                            <li className="left">
                                <img src="images/users/user-2.jpg" className="profile-photo-sm pull-left" />
                                <div className="chat-item">
                                    <div className="chat-item-header">
                                       
                                      
                                    </div>
                                   
                                </div>
                            </li>
                            <li className="right">
                                <img src="images/users/user-1.jpg" className="profile-photo-sm pull-right" />
                                <div className="chat-item">
                                    <div className="chat-item-header">
                                       
                                        
                                    </div>
                                   
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="send-message">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Type your message"  />
                        <span className="input-group-btn">
                            <button className="btn btn-default" type="button" >Send</button>
                        </span>
                    </div>
                </div>                                      
            </div>
                    </div>
                    <div className="clearfix" />
                </div>
            </div>

        </div>

    )
}