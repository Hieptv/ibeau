import { takeLatest, put } from "redux-saga/effects";
import { ActionTypeGetuser, getallusersuccess, getallusererror } from "./action";


function* Sagagetuser(queryParam) {
    const token = localStorage.getItem('isLogin');
    if (queryParam)
        try {
            const reqgetUser = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/get-all-user?${queryParam.payload}`,{
                method: "GET",
                headers: new Headers({
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'Authorization': `Bearer ${token}`
                })

            })
            const resgetUser = yield reqgetUser.json();
            
            yield put(getallusersuccess({docs: resgetUser.data.docs, page: resgetUser.data.page}));
            // yield put(getallusersuccess(resgetUser.data.page))
        }

        catch (error) {
            yield put(getallusererror(error));
        }
}
export default function* watchSagauser() {
    yield takeLatest(ActionTypeGetuser.GET_ALLUSER, Sagagetuser)
}