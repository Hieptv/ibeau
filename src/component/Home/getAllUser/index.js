import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { getalluser } from './action';
import Pagination from 'react-bootstrap/Pagination';
import qs from "qs";
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

AllUser.propTypes = {
    panigation: PropTypes.object.isRequired,
    handleClick: PropTypes.func
}

AllUser.defaultProps = {
    handleClick: null
}

function AllUser() {
    const [panigation, setpanigation] = useState({
        page: 1,

    });
    const [fillter, setfillter] = useState({
        page: 1
    })


    const dispatch = useDispatch()
    const queryParam = qs.stringify(fillter);

    useEffect(() => {
        dispatch(getalluser(queryParam))
    }, [queryParam])


    const handleClick = (e) => {

        setfillter({
            ...fillter,
            page: e,
        })
        setpanigation({
            ...panigation,
            page: e
        })
    }

    const data = useSelector(state => state.getAllUsers.listuser)
    const page = useSelector(state => state.getAllUsers.page)


    return (
        <div>
            <ul>
                {data && data.length > 0 &&
                    data.map((item, index) => 
                        <li key={index} className="follow-user" style={{listStyle:'none'}}>
                            <img src={item.avatar} className="profile-photo-sm pull-left"></img>
                            <div>
                                <h5><Link to={`/timeline?userid=${item._id}`}>{item.firstName} {item.lastName}</Link></h5>
                            </div>
                        </li>
                    )
                }
            </ul>
             
            <Pagination>
                <Pagination.First disabled={panigation.page <= 1} onClick={() => handleClick(panigation.page - 1)} />

                <Pagination.Last disabled={panigation.page >= 10} onClick={() => handleClick(panigation.page + 1)} />
            </Pagination>
        </div>
    )
}

export default AllUser
