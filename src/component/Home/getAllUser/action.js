export const ActionTypeGetuser = {
    GET_ALLUSER: 'GET_ALLUSER',
    GET_ALLUSER_SUCCESS: 'GET_ALLUSER_SUCCESS',
    GET_ALLUSER_ERROR: 'GET_ALLUSER_ERROR',

}
export const getalluser = (queryParam) => {
    return {
        type: ActionTypeGetuser.GET_ALLUSER,
        payload: queryParam
    }
}

export const getallusersuccess = (queryParam) => {
    return {
        type: ActionTypeGetuser.GET_ALLUSER_SUCCESS,
        payload: queryParam
    }
}

export const getallusererror = (queryParam) => {
    return {
        type: ActionTypeGetuser.GET_ALLUSER_ERROR,
        payload: queryParam
    }
}
