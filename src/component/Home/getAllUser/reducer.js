import {ActionTypeGetuser} from './action'

const queryParam = {
    listuser: [],
    loadPage: false,
    page: 1,
}
const getAllUsers = (state = queryParam, action) => {
    switch (action.type) {
        case ActionTypeGetuser.GET_ALLUSER:{
            return {
                ...state,
                loadPage: true
            }
        }
        case ActionTypeGetuser.GET_ALLUSER_SUCCESS:{
            return {
                ...state,
                listuser: action.payload.docs,
                loadPage: false,
                page:  action.payload.page
            }
        }  
        case ActionTypeGetuser.GET_ALLUSER_ERROR:{
            return {...state}
        }  
    
        default:
            return{
                ...state
            };
    }
};
export default getAllUsers;