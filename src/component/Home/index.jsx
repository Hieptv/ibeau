import React, { useEffect, useState } from 'react';
import Header from '../Header';
import NewsfeedContent from '../Newsfeed-content';
import { Route, Link, useLocation } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import Message from '../Message';
import { useSelector, useDispatch } from 'react-redux';
import Timeline from '../Timeline';
import People from './People';
import jwt_decode from 'jwt-decode';
import { getuserid} from '../Timeline/actions';
import {getuserID} from '../Timeline/reducer';
import { Spinner } from 'react-bootstrap';
import 'font-awesome/css/font-awesome.min.css';
import './style.scss';
import AllUser from './getAllUser/index';
import io from 'socket.io-client';
import listUser from './reducer';

const Home = () => {
    const token = localStorage.getItem('isLogin');
    const usernamess = localStorage.getItem('username');
    const [datas, setDatas] = useState([]);
    
    
   
    const dispatch = useDispatch();
    var decoded = jwt_decode(token);
    localStorage.setItem('id', decoded.userId);
    const userId = decoded.userId;
    const user_id = localStorage.getItem('id');

    useEffect(()=> {
        dispatch(getuserid(userId))
    },[])


    const data = useSelector(state => state.getuserID.listuserid)
    const loadingpage = useSelector(state => state.getuserID)
    var datauser = "";
    if (data) {
        datauser = (
            <>
                <Link to={`/timeline?userid=${data?._id}`} className="text-white">
                    <img src={data.avatar !== "" ? data.avatar : data.avatar === "" && data.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="profile-photo" />
                    <span className="username_infor">{data.firstName} {data.lastName}</span>

                </Link>
                <br></br>
                <a href="#" className="text-white">1222 Follower</a>
            </>
        )
    }

    useEffect(()=> {
        const socket = io(`http://192.168.0.144:3002`);
        socket.emit('online', {user_id: user_id});
        socket.on('list-user', (dataOnline) => {
            setDatas(dataOnline);
        })
    },[])

    useEffect(()=>{
        if(datas.length) return dispatch(listUser(datas))
    },[])

    const DataOnline = [...datas].filter(item => item?._id !== user_id);

    return (

        <div>

            <Header />
            
            <div id="page-contents">

                <div className="container">
                    <div className="row">

                        <div className="col-md-3">
                            <div className="profile-card mt-4">
                                {datauser}
                            </div>
                            <ul className="nav-news-feed">
                                <li><i className="icon ion-ios-paper" /><div><Link to={`/?userid=${data?._id}`}>My Newsfeed</Link></div></li>
                                <li><i className="icon ion-ios-people" /><div><Link to="/people">People Nearby</Link></div></li>
                                <li><i className="icon ion-chatboxes" /><div><Link to="/message">Message</Link> </div></li>
                            </ul>
                            <div id="chat-block">
                                <div className="title">Chat online</div>
                                <ul class="online-users list-inline">
                                    {DataOnline && DataOnline.length > 0 &&
                                        DataOnline.map((item, index) => 
                                            <li key={index}>
                                                <a href={`/message`} title="Linda Lohan">
                                                    <img src={item.avatar} alt="user" class="img-responsive profile-photo" />
                                                    <span class="online-dot"></span>
                                                </a>
                                            </li>
                                        )  
                                    }
                                </ul>
                            </div>
                        </div>

                        {loadingpage.loadpage === true ? (<Spinner animation="border" variant="primary" />) : (<></>)}

                        <div className="col-md-7">
                            <div>
                                <Switch>
                                    <Route path="/people" component={People} />
                                    <Route exact path="/">
                                        <NewsfeedContent />
                                    </Route>
                                    <Route path="/message">
                                        <Message />
                                    </Route>
                                </Switch>
                            </div>

                        </div>

                        <div className="col-md-3 static">
                            <div className="suggestions" id="sticky-sidebar">
                                <h4 className="grey">User follow</h4>
                                <AllUser />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



    )
}
export default Home;