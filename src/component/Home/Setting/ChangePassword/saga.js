import { ActionChangePassword } from "./action";
import Header from "../../../Header";
import { takeLatest } from "redux-saga/effects";
const user_id = localStorage.id;
const token = localStorage.isLogin;
function * SagaChangePassword(user) {
    console.log(user);
    try {
        const reqchangePass = yield fetch(`https://aht-social.herokuapp.com/api/v1/user/change-password/${user_id}`,{
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Bearer ${token}`
            }),
            body: JSON.stringify({oldPassword: user.payload.pass,newPassword: user.payload.newpass})
        })
        const reschangePass = yield reqchangePass.json();
        console.log(reschangePass);
    } catch (error) {
        console.log(error);
    }
}
export default function* WatchSagaChangePassword (){
    yield takeLatest (ActionChangePassword.CHANGE_PASSWORD, SagaChangePassword)
}