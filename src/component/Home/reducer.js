import {ActionUserOnline} from './action';

const userOnline = {
    online : []
}

const listUser  = ( state = userOnline, action ) => {
    switch(action.type){
        case ActionUserOnline.USERONLINE: {
            return {
                ...state
            }
        }
        case ActionUserOnline.USERONLINE_SUCCESS: {
            return {
                ...state,
                online: action.payload
            }
        }
        case ActionUserOnline.USERONLINE_ERROR: {
            return {
                ...state
            }
        }
        default:{
            return {
                ...state
            }
        }
    }
}

export default listUser