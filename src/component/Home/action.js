import { Accordion } from "react-bootstrap"

export const ActionUserOnline = {
    USERONLINE : "USERONLINE",
    USERONLINE_SUCCESS: "USERONLINE_SUCCESS",
    USERONLINE_ERROR: "USERONLINE_ERROR"
}

export const userOnline = (user) => {
    return {
        type: ActionUserOnline.USERONLINE,
        payload: user
    }
}

export const userOnlineSuccess = (user) => {
    return {
        type: ActionUserOnline.USERONLINE_SUCCESS,
        payload: user
    }
}

export const userOnlineError = (user) => {
    return {
        type: ActionUserOnline.USERONLINE_ERROR,
        payload: user
    }
}