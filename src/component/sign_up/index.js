import React, { useState } from 'react';
import { Form, Col, Row, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { signup } from './action';
import { useHistory } from 'react-router-dom';
export const Signup = () => {
    const [firstname, setfirstname] = useState('');
    const [lastname, setlastname] = useState('');
    const [user, setuser] = useState('');
    const [passwords, setpasswords] = useState('');
    const [email, setemail] = useState('');
    const [birthday, setbirthday] = useState('');
    const [checkmale, setcheckmale] = useState('');
    const dispatch = useDispatch();
    const history = useHistory();
    const statesignup = useSelector(state => state.signupreducer);
    const handlesignup = (e) => {
        e.preventDefault();
        if (firstname, lastname, user, passwords, birthday, checkmale) {
            dispatch(signup({
                firstname: firstname,
                lastname: lastname,
                username: user,
                password: passwords,
                email: email,
                birthday: birthday,
                gender: checkmale
            }))
            window.confirm("Đăng ký tài khoản thành công");
            history.push("/login")
        }
    }

    const backlogin = () => {
        history.push("/login")
    }
    return (
        <div className="d-flex container">
            <div className="w-50 bg-light"></div>

            <Form className="container mt-5 w-50">
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                        FirstName
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setfirstname(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                        LastName
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setlastname(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                        Username
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setuser(e.target.value)} />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                        Password
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="password" onChange={e => setpasswords(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                        Email
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setemail(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextPassword">
                    <Form.Label column sm="2">
                        Birthday
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="date" onChange={e => setbirthday(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group className="d-flex">
                    <Form.Check className="mr-3" type="checkbox" label="Male" onChange={(e) => setcheckmale('1')} />
                    <Form.Check type="checkbox" label="FeMale" onChange={(e) => setcheckmale('0')} />
                </Form.Group>
                <Button variant="primary" onClick={handlesignup}>Send Signup</Button>
                <Button variant="danger ml-3" onClick={backlogin}>Close</Button>
            </Form>
        </div>
    )
}