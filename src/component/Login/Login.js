import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {  useHistory, useLocation, Redirect, Link } from 'react-router-dom';
import {login} from './actions';
import { Spinner } from 'react-bootstrap';
import '../Login/style.scss';

function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    
    const dispatch = useDispatch();
    
    const isLogin = localStorage.getItem('isLogin');
    const history = useHistory();
    const [signup , setSignup] = useState('');
    const tokenLogin = useSelector(state => state.Login);
   
    let Location = useLocation();
    let {from} = Location.state || {from : {pathname: '/Home'}};
    const Loginuser = useSelector(state => state.Login);
    const user_id = localStorage.getItem('id')
    
   
    let inputnull = '';

    const handleSubmit = (e) => {
        e.preventDefault();
        if(username === '', password === ''){
            document.getElementById('inputnull').innerHTML = "(*) Bạn chua nhập tên tài khoản hoặc mật khẩu"
        }
        else{
            dispatch(login({username: username, password: password}));           
        }

    }

    if(Loginuser.isError){
        document.getElementById('inputnull').innerHTML = "Tài Khoản hoặc mật khẩu của bạn nhập sai"
    }
    
    if(isLogin){
        alert('Đăng nhập thành công');
        return (<Redirect to='/' />);
    }

    const handlesignup = () => {
        document.getElementById("inputnull").innerHTML = "_";
        history.push('/signup')
      }

    return (
        <div className="limiter">
            <div className="container-login100">
                <div className="wrap-login100">
                    <img src="./image/anhnen.png" />
                    <form className="login100-form validate-form" onSubmit={handleSubmit}>
                        <span className="login100-form-title mb-5">
                            Login to continue
                        </span>
                        <div className="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                            <input className="input100" type="text" name="email" onChange={(e) => setUsername(e.target.value)} placeholder="Email" />
                            <span className="focus-input100" />

                        </div>
                        <div className="wrap-input100 validate-input" data-validate="Password is required">
                            <input className="input100" type="password" name="pass" onChange={(e) => setPassword(e.target.value)} placeholder="Password" />
                            <span className="focus-input100" />

                        </div>
                        <div className="d-flex justify-content-between w-full pt-3 pb-3">
                            <div className="contact100-form-checkbox">
                                <input className="input-checkbox100" id="ckb1" type="checkbox" name="remember-me" />
                                <label className="label-checkbox100" htmlFor="ckb1">
                                    Remember me
                                </label>
                            </div>
                            <div>
                                <Link to="/forgot-password" className="txt1">
                                    Forgot Password?
                                </Link>
                            </div>
                            
                        </div>
                        <p id="inputnull" className="inputnull"></p>
                        <div className="container-login100-form-btn mt-3">
                            <button className="login100-form-btn"  type="submit">
                                Login
                            </button>
                            {tokenLogin.isloading === true ?(<Spinner animation="border" variant="primary" />):(<></>)}
                        </div>
                        <div className="text-center p-t-46 p-b-20 mt-3">
                            <span className="txt2">
                                or sign up using
                            </span>
                        </div>
                        <div className="login100-form-social d-lex text-center mt-3">
                            <a href="#" className="login100-form-social-item">
                                <i className="fab fa-facebook-f"></i>
                            </a>
                            <a href="#" className="login100-form-social-item ">
                                <i className="fab fa-twitter"></i>
                            </a>
                        </div>
                        <div className="container-login100-form-btn mt-4">
                            <button className="login100-form-btn sign-up" type="submit" onClick={handlesignup}>
                                Sign up
                            </button>
                        </div>
                    </form>
                    <div className="login100-more">
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login
