import { takeLatest, put } from "redux-saga/effects";
import {ActionType, loginError, loginSuccess} from './actions';



function* SagaGetLogin(user) {
    const token = '';
    console.log({user});
    try{
        const requestLogin = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/login`,{
            method : "POST",
            headers : new Headers({
                'Content-Type' : 'application/json',
                'Accept' : 'application/json',
            }),
            body: JSON.stringify({username: user.payload.username, password : user.payload.password})
        })
        const reponseLogin = yield requestLogin.json();
        console.log(reponseLogin)
        if(reponseLogin.status === true){
            localStorage.setItem('isLogin', reponseLogin.token)
            localStorage.setItem('username', user.payload.username)
        }
        else {
            yield put(loginError('error'))
        }
        yield put(loginSuccess(reponseLogin.token))
        
        
    }
    catch(error){
        yield put(loginError(error))
    }
}

export default function* watchSagaLogin() {
    yield takeLatest(ActionType.LOGIN, SagaGetLogin) 
}