export const ActionType = {
    LOGIN : 'LOGIN',
    LOGIN_SUCCESS : 'LOGIN_SUCCESS',
    LOGIN_ERROR : 'LOGIN_ERROR',
}

export const login = (list) => {
    console.log(list)
    return {
        type: ActionType.LOGIN,
        payload: list,
    }
}

export const loginSuccess = (list) => {
    return {
        type: ActionType.LOGIN_SUCCESS,
        payload: list,
    }
}

export const loginError = (list) => {
    return {
        type: ActionType.LOGIN_ERROR,
        payload: list
    }
}