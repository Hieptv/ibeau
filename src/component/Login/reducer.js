import {ActionType} from './actions';


const initialState = {
   user: [],
   isLoading: false,
   isError: '',
  };

const Login = (state = initialState, action) => {
    switch (action.type){
        case ActionType.LOGIN:{
            return {...state, isLoading: true}
        }
        case ActionType.LOGIN_SUCCESS: {
            return {...state, isError: '', isLoading: false}
        }
        case ActionType.LOGIN_ERROR: {
            return {...state, isError: action.payload, isLoading: false}
        }
        default: 
            return {...state}
    }
}

export default Login