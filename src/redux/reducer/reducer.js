import {combineReducers} from 'redux';
import Login from '../../component/Login/reducer';
import Signup from '../../component/sign_up/reducer';
import getuserID from '../../component/Timeline/reducer';
import loadAvatar from '../../component/Timeline/modal-avatar/reducer' ;
import loadImageCover from '../../component/Timeline/modal-imageCover/reducer';
import getusers from '../../component/Home/getAllUser/reducer';
import putImage from  '../../component/Timeline/image/reducer';
import getAllUsers from '../../component/Home/getAllUser/reducer';
import loadInformation from '../../component/Timeline/information/reducer';
import loadStory from '../../component/Timeline/story/reducer';
import followFriend from '../../component/Timeline/follow/reducer';
import getSearch from '../../component/Header/search/reducer';
import unfollowFriend from '../../component/Timeline/unfllow/reducer';
import listUser from '../../component/Home/reducer';


const rootReducer = combineReducers({
    Login: Login,
    Signup: Signup,
    getuserID: getuserID,
    loadAvatar : loadAvatar,
    loadImageCover: loadImageCover,
    getusers: getusers,
    putImage: putImage,
    getAllUsers: getAllUsers,
    loadInformation: loadInformation,
    loadStory: loadStory,
    followFriend: followFriend,
    getSearch: getSearch,
    unfollowFriend: unfollowFriend,
    listUser: listUser,
    
})

export default rootReducer;