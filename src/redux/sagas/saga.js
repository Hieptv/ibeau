import {all} from 'redux-saga/effects';
import watchSagaSignup from '../../component/sign_up/saga';
import watchSagauserid from '../../component/Timeline/saga';
import watchSagaLoadAvatar from '../../component/Timeline/modal-avatar/saga';
import WatchSagaChangePassword from '../../component/Home/Setting/ChangePassword/saga';
import watchSagaLoadImageCover from '../../component/Timeline/modal-imageCover/saga';
import watchSagauser from '../../component/Home/getAllUser/saga';
import watchSagaupimage from '../../component/Timeline/image/saga';
import watchSagaInformation from '../../component/Timeline/information/saga';
import watchSagaStory from '../../component/Timeline/story/saga';
import watchSagaFollow from '../../component/Timeline/follow/saga';
import watchSagasearch from '../../component/Header/search/saga';
import watchSagaUnFollow from '../../component/Timeline/unfllow/saga';
import watchSagaLogin  from '../../component/Login/sagas';


function* rootSaga(){
    yield all([
        watchSagaLogin(),
        watchSagaSignup(),
        watchSagauserid(),
        watchSagaLoadAvatar(),
        WatchSagaChangePassword(),
        watchSagaLoadImageCover(),
        watchSagauser(),
        watchSagaupimage(),
        watchSagaInformation(),
        watchSagaStory(),
        watchSagaFollow(),
        watchSagasearch(),
        // watchSagaUnFollow(),
        
    ])
};

export default rootSaga;