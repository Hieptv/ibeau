import React, { useEffect } from 'react';
import './App.css';
import Login from './component/Login/Login';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import setting from './component/Home/Setting';
import ForgotPassword from './component/Forgot_Passwork';
import Timeline from './component/Timeline';
import Home from './component/Home'
import PrivateRoute from './component/Home/PrivateRoute';
import { Signup } from './component/sign_up';


function App() {
  return (

    <Router>
        <Switch>
          <Route path="/login" component={Login}/>
          <Route path="/signup" component={Signup}/>
          
          <Route path="/setting" component={setting}/>
            
          <Route path = "/forgot-password" component={ForgotPassword} />
          <PrivateRoute  path ="/timeline" component={Timeline} />
          
          <PrivateRoute exact path ="/" component={Home} />
        
        </Switch>
    </Router>
  );
}

export default App;
